const EventEmiter = require('events').EventEmitter;

class ErrorEmitter extends EventEmiter {

    constructor() {
        super();

        this.on('error', error => {
            console.log(error);
        });
    }
}


const emiter = new ErrorEmitter();
emiter.emit('error', `this is a fucking error`);
