#!/bin/bash

DBHOST='172.19.0.2'
DBPASSWORD='ok'

echo "Bootstrapping..."

#create a network
NETWORK_CREATED=`docker network ls -f 'name=intranet' -q`
DB_SERVER_STARTED=`docker container ls -f 'name=mariadb10.3.8' -q`

if [[ "" = $NETWORK_CREATED ]]; then
    echo "Creating network intranet"
    docker network create --subnet 172.19.0.0/16 --gateway 172.19.0.1 intranet
fi

if [[ "" = $DB_SERVER_STARTED ]]; then
    echo "Running mariadb server"
    docker run --rm --name mariadb10.3.8 -e MYSQL_ROOT_PASSWORD=$DBPASSWORD -d --net intranet --ip $DBHOST mariadb:10.3.8
fi

echo "Done!!!"