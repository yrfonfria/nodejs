#!/bin/bash

echo "Shutting down environment ..."

docker stop mariadb10.3.8
docker network rm intranet

echo "Done!!!"