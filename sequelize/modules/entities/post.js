
module.exports = (sequelize, DataTypes) => {
    const Post = sequelize.define(
        'Post', 
        {
            id: {
                type: DataTypes.INTEGER,
                field: 'id',
                primaryKey: true
            },
            userId: {
                type: DataTypes.INTEGER,
                field: 'user_id'
            },
            categoryId: {
                type: DataTypes.INTEGER,
                field: 'category_id'
            },
            subcategoryId: {
                type: DataTypes.INTEGER,
                field: 'subcategory_id'
            },
            company: {
                type: DataTypes.STRING,
                field: 'company'
            },
            name: {
                type: DataTypes.STRING,
                field: 'name'
            }
        }, 
        {
            tableName: 'post',
            sequelize: sequelize
        });

        return Post;
};