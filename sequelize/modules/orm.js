const Sequelize = require('sequelize');
const db = new Sequelize('transport', 'root', 'ok', {
    host: '172.19.0.2',
    dialect: 'mariadb',
    operatorsAliases: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    define: {
        timestamps: false
    }
});

module.exports = {
    db: db,
    sequelize: Sequelize
};
