const Sequelize = require('sequelize');
const db = new Sequelize('transport', 'root', 'ok', {
    host: '172.18.0.2',
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    define: {
        timestamps: false
    }
});

const Post = require('./modules/entities/post')(db, Sequelize);

Post.findAll().then(results => {
    results.forEach(val => {
        console.log(val.id, val.userId, val.categoryId, val.subcategoryId, val.company, val.name);
    });
});