let p = new Promise((resolve, reject) => {
    setTimeout(() => resolve('This is the resolution for this promise'), 2500);
});

console.log(`HEAD: This is a very simple example about how promises work`);
p.then((successMsg)=> console.log(`Yay!!!! promise resolved ${successMsg}`));
console.log(`This text will appear just below the head, then whatever can happen, let's try it ...`);