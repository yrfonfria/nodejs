const fs = require('fs');
const http = require('http');
const path = require('path');

const CONTENT = '/content/';
const HTTP_OK = 200;
const HTTP_BAD_REQUEST = 400;


let s = http.createServer((req, res) => {

    if(req.url.substring(0, 9) == CONTENT) {
        (function(file, req) {
            fs.exists(__dirname + `/contents/${file}`, (exists) => {
                if(!exists) {
                    res.writeHead(HTTP_OK, { "Content-Type": 'text/plain' });
                    res.write(` The requested resource does not exists on this server`);
                    res.end();
                    return;
                }
            });

            const rs = fs.createReadStream(__dirname + `/contents/${file}`);

            rs.on('error', (error) => {
                res.end();
            });

            const ct = ctForFile(file);
            res.writeHead(HTTP_OK, { "Content-Type": ct });
            rs.pipe(res);

        })(req.url.substring(9), req);
    }
    else {
        res.writeHead(HTTP_BAD_REQUEST, { "Content-Type": 'text/plain'});
        res.write("Bad Api Call");
        res.end();
    }

    
});

function ctForFile(file) {
    let ext = path.extname(file);

    switch(ext.toLowerCase()) {
        case '.html': return 'text/html';
        case '.json': return 'application/json';
        case '.js': return 'application/javascript';
        case '.jpg': return 'image/jpg';
        case '.png': return 'image/png';
        default: return 'text/plain';
    }
}
s.listen(3000);
