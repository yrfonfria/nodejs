const events = require('events');
const STEP = 2000;

class Downloader extends events.EventEmitter {
    constructor() {
        super();
    }

    downloadUrl(path) {
        let self = this;
        self.url = path;
        self.emit('start', path);
        setTimeout(() => { self.emit('end', path)}, STEP);
    }
}

let d = new Downloader();

d.on('start', (path) => { console.log(`Download of ${path} has been started`); });
d.on('end', (path) => { console.log(`Download of ${path} finished`); });

d.downloadUrl('http://symfony.com');