const BUFF_SIZE = 10000;

let b = new Buffer(BUFF_SIZE);
const str = "中共欲取消对国家主席的任期限制";

b.write(str);
console.log(`the content of buffer is ${b.toString('utf8')} and the size is: ${b.length} however the real string size is ${str.length}`);
console.log(
    `the proper way to get the byte length of a string is calling Buffer.byteLength(string) as in ${Buffer.byteLength(str)} 
because not always the amoutn of chars in s atring mathc the number of bytes`
);