const fs = require('fs');
const http = require('http');
const path = require('path');
const url = require('url');
const albumMgr = require('./album_mgr');

//Static folders allowed ...
const CONTENT       = 'content';
const PAGES         = 'page';
const TEMPLATES     = 'templates';
const ALBUMS        = 'albums';
const ALBUM         = 'album';
const NODE_MODULES  = 'node_modules';

//Static urls allowed
const URL_ALBUMS     = '/albums.json';

//Positions inside parsed URL ...
const MATCH_FOLDER  = 2;
const MATCH_RESOURCE = 3;

//HTTP Codes ...
const HTTP_OK                       = 200;
const HTTP_BAD_REQUEST              = 400;
const HTTP_NOT_FOUND                = 404;
const HTTP_INTERNAL_SERVER_ERROR    = 500;

const RESOURCES_DIR = __dirname + '/../../resources';
const PAGES_EXTENSION = '.js';

function handleAlbums(req, res) {
    albumMgr.albums(RESOURCES_DIR, (error, albums) => {
        if(error) {
            sendError(error, res);
            return;
        }

        let response = [];
        Object.getOwnPropertyNames(albums).forEach((prop) => {
            response.push({ name: albums[prop].name, path: albums[prop].path });
        });

        res.writeHead(HTTP_OK, { 'Content-Type': 'application/json'});
        res.write(JSON.stringify(response));
        res.end();
    });
}

function handleAlbum(req, res) {

    let matches = /((\w+).json)/.exec(req.parsedUrl.pathname);

    if(matches.length == 0 || !matches[2]){
        sendError({ error: 'bad_request', message: 'not a json resource'});
        return;
    }

    albumMgr.photoAlbum(RESOURCES_DIR, matches[2], (err, album) => {
        if(err){
            sendError(err, res);
            return;
        }

        let a = { name: album.name };

        album.photos((err, photos) => {
            if(err){
                sendError(err, res);
                return;
            }

            a.photos = photos;
            
            res.writeHead(HTTP_OK, { 'Content-Type': 'application/json'});
            res.write(JSON.stringify(a));
            res.end();
        });
    });
}

function sendError(error, res) {
    let code;
    switch(error.error) {
        case 'no_such_album':
            code = HTTP_NOT_FOUND;
            break;
        case 'bad_request':
            code = HTTP_BAD_REQUEST;
            break;
        default:
            code = HTTP_INTERNAL_SERVER_ERROR;
            break;
    }

    res.writeHead(code, { "Content-Type": 'application/json'});
    res.write(error.message);
    res.end();
}

/**
 * Serve a static file from our app or HTTP Error not found.
 * 
 * @param {String} file 
 * @param {String} folder 
 * @param {ServerResponse} res 
 */
function serveStaticFile(file, folder, res) {
    fs.exists(__dirname + `/${folder}/${file}`, (exists) => {
        if(!exists) {
            res.writeHead(HTTP_NOT_FOUND, { "Content-Type": 'text/plain' });
            res.write(` The requested resource does not exists on this server`);
            res.end();
            return;
        }
    });

    const rs = fs.createReadStream(__dirname + `/${folder}/${file}`);

    rs.on('error', (error) => {
        res.end();
    });

    const ct = ctForFile(file);
    res.writeHead(HTTP_OK, { "Content-Type": ct });
    rs.pipe(res);

}

/**
 * Guess the possble mime-type for the file being served.
 * @param {String} file filename 
 */
function ctForFile(file) {
    let ext = path.extname(file);

    switch(ext.toLowerCase()) {
        case '.html': return 'text/html';
        case '.json': return 'application/json';
        case '.js': return 'application/javascript';
        case '.jpg': return 'image/jpg';
        case '.png': return 'image/png';
        case '.css': return 'text/css';
        default: return 'text/plain';
    }
}

/**
 * Serves the requested page to the user or just a not found HTTP Error.
 * 
 * @param {String} page the static page to serve
 * @param {String} source if you want to override the js to be loaded, then pass the js name here.
 * @param {IncomingMessage} req request  
 * @param {ServerResponse} res 
 */
function servePage(page, source, req, res) {
    const EXTGROUP = 3;
    const RSRCGROUP = 1;

    let spread      = /^((\w+|_|-)*)(.js)?$/.exec(page);
    let extIncluded = spread[EXTGROUP];
    let resource    = spread[RSRCGROUP];
    let fileName;

    if(!source) {
        source      = resource;
        resource    = '';
        fileName    = __dirname + (extIncluded ? `/${CONTENT}/${page}` : `/${CONTENT}/${page}${PAGES_EXTENSION}`);
    }
    else {
        fileName = `${__dirname}/${CONTENT}/${source}${PAGES_EXTENSION}`;
    }

    //there is no such page to be served.
    fs.exists(fileName, (exists) => {
        if(!exists) {
            res.writeHead(HTTP_NOT_FOUND, { "Content-Type": 'text/plain' });
            res.write(` The requested resource does not exists on this server`);
            res.end();
            return;
        }
    });

    fs.readFile(`${__dirname}/${TEMPLATES}/base.html`, (error, content) => {

        if(error) {
            sendError(error, res);
            return;
        }

        content = content.toString('utf8');
        content = content
            .replace(/{{PAGE_NAME}}/, source)
            .replace(/{{RESOURCE_NAME}}/, resource);

        res.writeHead(HTTP_OK, { 'Content-Type': 'text/html'} );
        res.end(content);
    });
}

/** 
 * Definition of this primitive server and it's routing system.
 */
let s = http.createServer((req, res) => {

    let resource;
    const urlPattern    = /^(\/(\w+)\/)?(.*)/;
    req.parsedUrl       = url.parse(req.url);
    resource            = urlPattern.exec(req.parsedUrl.pathname);

    let foldersPattern = new RegExp(`${CONTENT}|${TEMPLATES}|${NODE_MODULES}`);
    if(resource) {
        switch(true) {
            case (resource[MATCH_FOLDER] === PAGES):
                servePage(resource[MATCH_RESOURCE], null, req, res);
                break;
            case (resource[MATCH_FOLDER] === ALBUM):
                servePage(resource[MATCH_RESOURCE], 'album', req, res);
                break;
            case (foldersPattern.test(resource[MATCH_FOLDER])):
                serveStaticFile(resource[MATCH_RESOURCE], resource[MATCH_FOLDER], res);
                break;
            case (resource[MATCH_FOLDER] == ALBUMS):
                handleAlbum(req, res);
                break;
            case (resource[MATCH_RESOURCE] === URL_ALBUMS):
                handleAlbums(req, res);
                break;
            default:
                sendError({ error: 'bad_request', message: 'Bad Api Call' }, res);
                break;
        }
    }
    else
        sendError({ error: 'bad_request', message: 'Bad Api Call' }, res);
});

s.listen(3000);
