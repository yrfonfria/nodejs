const p     = require('path');
const fs    = require('fs');

class Album {

    constructor(path) {
        this.name = p.basename(path);
        this.path = path;
    }

    photos(callback) {
        const _self = this;

        fs.readdir(_self.path, (err, files) => {
            if (err) {
                if (err.code == 'ENOENT') {
                    callback(_noSuchAlbum(name));
                }
                else {
                    callback({ error: 'file_error', message: JSON.stringify(err)});
                }
                return;
            }
    
            let photos = [];

            (function it(index) {
                if (index == files.length) {
                    callback(null, photos);
                    return;
                }
    
                fs.stat(`${_self.path}/${files[index]}`, (err, stats) => {
                    if (err) {
                        callback({ error: 'file_error', message: JSON.stringify(err)});
                        return;
                    }
    
                    if (stats.isFile()) photos.push({ name: files[index], path: `${_self.path}/${files[index]}`});
                    it(++index);
                });
            })(0);
        });
    }

}

function _noSuchAlbum(name) {
    return { error: 'no_such_album', message: `The specified album ${name} does not exists` };
}

module.exports = {
    getAlbum(path) {
        return new Album(path);
    }
};

