$(function(){
    $.getJSON(`/albums/${config.albumName}.json`, (data, status, jqXHR) => {
        console.log(data);
        $.get('/templates/album.html', (template, status, jqXHR) => {
            console.log(template);
            let page = Mustache.to_html(template, {album: data});
            $('body').html(page);
        })
    });
});