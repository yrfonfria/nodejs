$(function(){
    $.get('/templates/home.html', (d, s, jqXhr) => {
        $.getJSON('/albums.json', (albums, s2, jqXhr2) => {
            console.log(`Retrieved template ${d}`);
            console.log(`Retrieve data ${JSON.stringify(albums)}`);
            let renderedPage = Mustache.to_html(d, { albums: albums});
            $('body').html(renderedPage);
        });
    }); 

});