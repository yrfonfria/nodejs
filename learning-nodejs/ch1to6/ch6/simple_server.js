const fs = require('fs');
let contents = "";

rs = fs.createReadStream('./ch6/simple_server.js');

rs.on('readable', () => {
    let str;
    let d = rs.read();
    if(d) {
        if(typeof d == 'string') {
            str = d
        }
        else if (typeof d == 'object' && d instanceof Buffer) {
            str = d.toString('utf8');
        }

        if(str) {
            contents += str;
        }
    }
});

rs.on('end', () => {
    console.log("Read in file contents: ");
    console.log(contents.toString('utf8'));
});

rs.on('error', () => { console.log('Something bad happened ... :-(')});