
//algorithm yto compute the date offset based on weekdays.. 
const SUNDAY = 7;
const WEEK = 7;
const DAY_MILLISECONDS = 86400000;

const DAYS_OF_WEEEK = [ 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday' ];

let baseDates = ['2017-12-11', '2017-12-12', '2017-12-13', '2017-12-14', '2017-12-15', '2017-12-16', '2017-12-17'];
let dayOfWeeks  = [1,2,3,4,5,6,7];

let executor = function (d, dow, after) {
  let date = new Date(d);
  let current  = date.getUTCDay();
  let offset;

  current = current == 0 ? SUNDAY : current;

  if(after) {
    switch(true) {
      case (current > dow): offset = WEEK - (current - dow) ; break;
      case (current < dow): offset = dow - current; break;
      default: offset = WEEK; break;
    }
  }
  else {
    switch(true) {
      case (current > dow): offset = current - dow; break;
      case (current < dow): offset = WEEK + (current - dow); break;
      default: offset = WEEK; break;
    }

    offset *= -1;
  }

  date.setTime(date.getTime() + offset * DAY_MILLISECONDS);

  let result =  date.getUTCFullYear() + (date.getUTCMonth() < 9 ? "-0" : "-") + (date.getUTCMonth() + 1) + (date.getUTCDate() < 10 ? "-0" : "-") + date.getUTCDate();
  let dayOfWeek = DAYS_OF_WEEEK[date.getUTCDay()];

  console.log(`|\t\t${current}\t|\t\t${dow}\t|\t\t${after}\t|\t\t${offset}\t|\t\t${result}\t|\t\t${dayOfWeek}\t\t|`);

};

//main program ...
baseDates.forEach((d) => {
    let date = new Date(d);
    let day = DAYS_OF_WEEEK[date.getUTCDay()];

    console.log(`\n\nComputing for ${d} day is ${day}\n`);
    /*console.log('AFTER >>>');
    console.log(`|\t\tcur\t|\t\tdow\t|\t\tafter\t|\t\toffset\t|\t\tresult\t|\t\tDay Of Week\t\t|`);

    dayOfWeeks.forEach((e) => {
        executor(d, e, true);
    });*/

    console.log('BEFORE <<<');
    console.log(`|\t\tcur\t|\t\tdow\t|\t\tafter\t|\t\toffset\t|\t\tresult\t|\t\tDay Of Week\t\t|`);

    dayOfWeeks.forEach((e) => {
        executor(d, e, false);
    });
});
