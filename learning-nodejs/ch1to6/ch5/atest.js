const amgr = require('./album_mgr');

amgr.albums(`${__dirname}/../resources`, (err, albums) => {
    if(err) {
        console.log(`Unecpected error: ${JSON.stringify(err)}`);
        return;
    }

    (function it(idx){
        if (idx == albums.length) {
            console.log('Done');
            return;
        }

        albums[idx].photos((err, photos) => {
            if(err){
                console.log(`Error reading album: ${JSON.stringify(err)}`);
                return;
            }

            console.log(albums[idx].name);
            console.log(photos);
            console.log("\n-------------------------------------\n");
            it(++idx);
        });
    })(0);
});