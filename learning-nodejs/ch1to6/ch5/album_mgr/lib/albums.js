const fs            = require('fs');
const album         = require('./album.js');

const ALBUMS_DIR = 'albums';


module.exports = {
    version: '1.0.0',

    albums(root, callback) {
        
        const rootDir = `${root}/${ALBUMS_DIR}`;
        fs.readdir(rootDir, (err, files) => {
            if (err) {
                if (err.code == 'ENOENT') callback({ error: "file_error", message: JSON.stringify(err)});
                return;
            }
    
            let dirs = [];
    
            //here filter only directories using stat.
            (function it(index) {
                if (index == files.length) {
                    callback(null, dirs);
                    return;
                }
    
                fs.stat(`${rootDir}/${files[index]}`, (err, stats) => {
                    if (err) {
                        callback({ error: 'file_error', message: JSON.stringify(err)});
                        return;
                    }
    
                    if (stats.isDirectory()) {
                        let path = `${rootDir}/${files[index]}`;
                        dirs.push(album.createAlbum(path));
                    }
                    it(++index);
                });
    
            })(0);
        });
    }
};
