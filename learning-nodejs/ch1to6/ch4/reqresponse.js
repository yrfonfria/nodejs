const http = require('http');

http.createServer((req,res) => {

    console.log('----------------------------------------------------------------');
    console.log(req);
    
    res.writeHead(200, { 'Content-Type': 'application/json'});
    res.end(JSON.stringify({error: null}) + "\n");
}).listen(8080);