const http = require('http');
const fs = require('fs');
const url = require('url');

const HTTP_CODES = {
    OK: 200,
    SERVICE_UNAVAILABLE: 503,
    RESOURCE_NOT_FOUND: 404,
    BAD_REQUEST: 400,
    INTERNAL_SERVER_ERROR: 500
};

const HEADERS = {
    content_type_json: { "Content-Type": "application/json" }
};

function loadAlbumList(callback) {
    const resourceDir = __dirname + '/../resources/albums';

    fs.readdir(resourceDir, (err, files) => {
        if (err) {
            if (err.code == 'ENOENT') callback(make_error("file_error", JSON.stringify(err)));
            return;
        }

        let dirs = [];

        //here filter only directories using stat.
        (function it(index) {
            if (index == files.length) {
                callback(null, dirs);
                return;
            }

            fs.stat(`${resourceDir}/${files[index]}`, (err, stats) => {
                if (err) {
                    callback(makeError('file_error', JSON.stringify(err)));
                    return;
                }

                if (stats.isDirectory()) dirs.push({ name: files[index] });
                it(++index);
            });

        })(0);
    });
}

function loadAlbum(name, page, size, callback) {
    const resourceDir = __dirname + '/../resources/albums/' + name;

    fs.readdir(resourceDir, (err, files) => {
        if (err) {
            if (err.code == 'ENOENT') {
                callback(noSuchAlbum(name));
            }
            else {
                callback(makeError('file_error', JSON.stringify(err)));
            }
            return;
        }

        let photos = [];

        (function it(index) {
            if (index == files.length) {
                let p;
                p = photos.splice(page * size, size);
                callback(null, { album_name: name, photos: p });
                return;
            }

            fs.stat(`${resourceDir}/${files[index]}`, (err, stats) => {
                if (err) {
                    callback(makeError('file_error', JSON.stringify(err)));
                    return;
                }

                if (stats.isFile()) photos.push({ name: files[index], path: `${resourceDir}/${files[index]}`});
                it(++index);
            });
        })(0);
    });
}

function handleListAlbums(req, res) {
    loadAlbumList((err, list) => {
        if (err) {
            outputFailure(res, HTTP_CODES.INTERNAL_SERVER_ERROR, err);
            return;
        }

        outputSuccess(res, { albums: list });
    });
}


function handleGetAlbum(req, res) {

    const qp        = req.parsed_url.query;
    const coreUrl   = req.parsed_url.pathname;
    let pageNum     = qp ? parseInt(qp.page) : 0;
    let pageSize    = qp ? parseInt(qp.page_size) : 1000;

    pageNum     = isNaN(pageNum) ? 0 : pageNum;
    pageSize    = isNaN(pageSize) ? 1000 : pageSize;

    const albumName = coreUrl.substr(7, coreUrl.length - 12);

    loadAlbum(albumName,pageNum, pageSize, (err, album) => {
        switch (true) {
            case (err && err.code == 'no_such_album'):
                outputFailure(res, HTTP_CODES.RESOURCE_NOT_FOUND, err);
                break;
            case (err):
                outputFailure(res, HTTP_CODES.INTERNAL_SERVER_ERROR, err);
                break;
            default:
                outputSuccess(res, { data: album });
                break;
        }
    });
}

function handleRenameAlbum(req, res) {
    const coreUrl = req.parsed_url.pathname;

    const parts = coreUrl.split('/');
    
    if(parts.length != 4 ) {
        outputFailure(res, HTTP_CODES.BAD_REQUEST, invalidResource());
        return;
    }

    const albumName = parts[2];
    let requestBody = '';
    req.on('readable', () => {
        let data = req.read();
        if(data) {
            if(typeof data == 'string') {
                requestBody += data;
            }
            else if(typeof data == 'object' && data instanceof Buffer) {
                requestBody += data.toString('utf8');
            }
        }
    });

    req.on('end', () => {
        let albumData;
        if(requestBody) {
            try {
                albumData = JSON.parse(requestBody);
                
                //got wrong body...
                if(!albumData.album_name) {
                    outputFailure(res, HTTP_CODES.BAD_REQUEST, badJson());
                    return;
                }
            }
            catch(e) {
                //got some body but not a valid json ...
                outputFailure(res, HTTP_CODES.BAD_REQUEST, badJson());
                return;
            }

            //perform the rename here.
            outputSuccess(res, { data: `renamed ok. New name is ${albumData.album_name}`});

        }
        else {
            outputFailure(res, HTTP_CODES.BAD_REQUEST, badJson())
        }
    });
}

function makeError(err, msg) {
    let e = new Error(msg);
    e.code = err;
    return e;
}

function outputSuccess(res, data) {
    res.writeHead(HTTP_CODES.OK, HEADERS.content_type_json);
    res.end(JSON.stringify({ error: null, data: data }) + "\n");
}

function outputFailure(res, code, err) {
    let outCode = err.code ? err.code : code;
    res.writeHead(code, HEADERS.content_type_json);
    res.end(JSON.stringify({ error: outCode, message: err.message }) + "\n");
}

function invalidResource() { return makeError('invalid_resource', 'the requested resource does not exists'); }
function badJson() { return makeError('bad_json_format', 'bad json format in request'); } 
function noSuchAlbum(name) { return makeError('no_such_album', 'the specified album does not exists'); }

http.createServer((req, res) => {
    console.log(`INCOMING REQUEST: ${req.method} ${req.url}`);

    req.parsed_url = url.parse(req.url, true);
    let core_url = req.parsed_url.pathname;

    console.log(req.parsed_url);

    switch (true) {
        case (core_url == '/albums.json'):
            handleListAlbums(req, res);
            break;
        case (core_url.substr(core_url.length - 12) == '/rename.json' && req.method.toLowerCase() == 'post'):
            handleRenameAlbum(req, res);
            break;
        case (core_url.substr(0, 7) == '/albums' && core_url.substr(core_url.length - 5) == '.json'):
            handleGetAlbum(req, res);
            break;
        default:
            outputFailure(res, HTTP_CODES.RESOURCE_NOT_FOUND, invalidResource())
            break;
    }
}).listen(8080);