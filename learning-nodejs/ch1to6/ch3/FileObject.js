const fs = require('fs');

class FileObject {

    constructor(fileName){
        this.fileName = fileName;
    }

    fileExists(callback) {
        console.log(`About to open: ${this.fileName}`);
        fs.open(this.fileName, 'r', (err, handle) => {
                if(err){
                    console.log(`Can't open ${this.fileName}` );
                    callback({
                        code: 'FAIL',
                        message: `Unable to open file: ${this.fileName}`
                    }, false);
                    return;
                }

                fs.close(handle, (err) => { 
                    console.log(`Error closing: ${this.fileName}`);
                });
                callback(null, true);
            }
        );
    }    
}


const f = new FileObject(__dirname + '/file_that_not_exists');

f.fileExists((err, result) => {
    if(result){
        console.log('File was found');
    }
    else {
        console.log('File was not found');
        console.log(err);
    }
});