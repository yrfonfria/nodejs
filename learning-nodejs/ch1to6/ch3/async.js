const fs = require('fs');

let buf = new Buffer(100000);

fs.open(__dirname + '/info.txt', 'r', 
    (err, handle) => {

        if(err) {
            console.log(`Error: ${err.code} ${err.message}`);
            return;
        }

        fs.read(handle, buf, 0, 100000, null, 
            (err, length) => {
            
                if(err){
                    console.log(`Error: ${err.code} ${err.message}`);
                    return;
                }
            
                console.log("Startings async programming sample\n\n");    
                console.log(buf.toString('utf8', 0, length));
                fs.close(handle, () => { console.log("\n\nEnded async programming"); })
            }
        );
    }
);



