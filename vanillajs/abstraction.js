function Circle(radius) {
    this.radius = radius;
    let location = {x: 0, y: 0};
    
    let computeLocation = function() {
        //...
        console.log(location);
    }

    this.draw = function() { 
        computeLocation();
        console.log(`draw`); 
    
    }

    Object.defineProperty(this, 'defaultLocation', {
        get: function(){ return location },
        set: function(value){
            location = value;
        }
    });

}

const createCircle = (radius) => {

    return {
        radius,
        draw: function() {
            console.log(`draw called`);
        }
    };
}


const AnotherCircle = new Function('radius', 'diameter', `
    this.radius = radius;
    this.diameter = diameter;
    this.draw = function(){ console.log('called draw'); }
`);

const circle4 = new Object();

const circle = new Circle(1); 
const circle2 = new Circle(2);
const circle3 = createCircle(3);
const circle5  = new AnotherCircle(5, 10);

console.log(`circle 1 location`, circle.defaultLocation);
console.log(`circle 2 location`, circle2.defaultLocation);

circle.computeLocation = 1;

console.log(`Changing the location of circle 1`);
circle.defaultLocation = {x: 10, y: 20 };

console.log(`printing them back again`);
console.log(`circle 1 location`, circle.defaultLocation);
console.log(`circle 2 location`, circle2.defaultLocation);
