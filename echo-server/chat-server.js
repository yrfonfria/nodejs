const EventEmmitter = require('events').EventEmitter;
const net = require('net');

/**
 * The channel in the chat .....
 */
class Channel extends EventEmmitter {

    constructor() {
        super();
        this.clients = {};
        this.subscriptions = {};

        /**
         * Handles the channel joins.
         */
        this.on('join', (id, client) => {
            this.clients[id] = client;  //store the socket in a list.
            
            /**
             * setup the broadcast handler for this socket ....
             */
            this.on('broadcast', (sender, message) => {
                if(this.clients[id] && sender !== id)
                    this.clients[id].write(`[${sender}]: ${message}\n`);
            });
            
            //log the new connection in the terminal on the server side
            console.log(`A new client has joined the party with id: ${id}\n`);
            
            //announce the new user to existing ones...
            this.emit('broadcast', id, `'${id}' have joined the party`);
        });


        //handle the leave of user from the channel .....
        this.on('leave', id => {
            delete this.clients[id];
        })
    }
}

//create the channel
const channel = new Channel();

//create the server ....
const server = net.createServer(socket => {
    const id = `${socket.remoteAddress}:${socket.remotePort}`;
    channel.emit('join', id, socket);

    socket.on('data', data => {
        channel.emit('broadcast', id, data);
    });

    socket.on('close', () => {
        channel.emit('leave', id);
    });
})

server.listen(8888);
